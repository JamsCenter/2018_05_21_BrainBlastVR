﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueToObject : MonoBehaviour {
    public Transform m_center;
    public LayerMask m_gunOn;
    public float m_maxDistance = 1;

    public bool m_hasNearObject;

	// Use this for initialization
	void Update () {
        m_hasNearObject = IsThereObjectAround();

    }
	
	// Update is called once per frame
	public bool IsThereObjectAround () {

        bool hasObject = false;
        hasObject |= Physics.OverlapSphere(m_center.position, m_maxDistance, m_gunOn).Length>0;


        //hasObject |= CheckForObject(m_center.position, m_center.up);
        //hasObject |= CheckForObject(m_center.position, -m_center.up);
        //hasObject |= CheckForObject(m_center.position, m_center.right);
        //hasObject |= CheckForObject(m_center.position, -m_center.right);
        //hasObject |= CheckForObject(m_center.position, m_center.forward);
        //hasObject |= CheckForObject(m_center.position, -m_center.forward);


        //hasObject |= CheckForObject(m_center.position, new Vector3(1, 1, 1));
        //hasObject |= CheckForObject(m_center.position, new Vector3(-1, -1, -1));

        //hasObject |= CheckForObject(m_center.position, new Vector3(1, 1, -1));
        //hasObject |= CheckForObject(m_center.position, new Vector3(1, -1, -1));
        //hasObject |= CheckForObject(m_center.position, new Vector3(-1, 1, -1));

        //hasObject |= CheckForObject(m_center.position, new Vector3(1, -1, 1));
        //hasObject |= CheckForObject(m_center.position, new Vector3(1, -1, -1));
        //hasObject |= CheckForObject(m_center.position, new Vector3(-1, -1, 1));


        //hasObject |= CheckForObject(m_center.position, new Vector3(-1, 1, 1));
        //hasObject |= CheckForObject(m_center.position, new Vector3(-1, 1, -1));
        //hasObject |= CheckForObject(m_center.position, new Vector3(-1, -1, 1));

        return hasObject;

    }

    private bool CheckForObject(Vector3 position, Vector3 direction)
    {
        bool hasHit = Physics.Raycast(new Ray(position, direction), m_maxDistance, m_gunOn);
        Debug.DrawRay(position, direction*m_maxDistance, hasHit?Color.green:Color.grey);
        return hasHit;


    }
}
