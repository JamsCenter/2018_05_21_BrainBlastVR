﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorPosition : MonoBehaviour {
    public Transform m_mirrorPosition;
    public float amplify=2;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.Lerp(transform.position, m_mirrorPosition.position, Time.deltaTime * amplify);
        transform.rotation = Quaternion.Lerp(transform.rotation, m_mirrorPosition.rotation, Time.deltaTime * amplify);

    }
}
