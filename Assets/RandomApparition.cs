﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RandomApparition : MonoBehaviour {

    public float m_firstTime=0;
    public float m_minTime=0.5f;
    public float m_maxTime=20;
    public float m_livingTime=2;
    public UnityEvent m_on;
    public UnityEvent m_off;


    IEnumerator Start () {

        m_on.Invoke(); 
        yield return new WaitForSeconds(m_firstTime);

        while (true) {

            m_off.Invoke();
            float waitFor = UnityEngine.Random.Range(m_minTime, m_maxTime);
            yield return new WaitForSeconds(waitFor);
            m_on.Invoke();
            yield return new WaitForSeconds(m_livingTime);
           

        }

	}

}
