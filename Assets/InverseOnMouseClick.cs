﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InverseOnMouseClick : MonoBehaviour {

    public GameObject m_target;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(0))
            m_target.SetActive(!m_target.activeSelf);
	}
}
