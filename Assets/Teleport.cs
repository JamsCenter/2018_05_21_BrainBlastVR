﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Teleport : MonoBehaviour
{
    public Transform m_cursor;
    public Transform m_affected;
    public LayerMask m_canTeleportOn;
    public OnTeleportation d_onTeleport;
    public OnTeleportEvent d_onTeleportEvent;
    public delegate void OnTeleportation(Vector3 from, Vector3 where, RaycastHit hit);
    [System.Serializable]
    public class OnTeleportEvent : UnityEvent<Vector3, Vector3, RaycastHit> { } 
    
    public void CheckForTarget() {
        Vector3 currentPosition = m_affected.position;
        Vector3 screenPointer = Input.mousePosition;
        Ray ray =  new Ray(m_cursor.position, m_cursor.forward) ;

        RaycastHit hit;

        bool hasHit = Physics.Raycast(ray, out hit, float.MaxValue, m_canTeleportOn);
        if (!hasHit)
        {
            ray = Camera.main.ScreenPointToRay(screenPointer);
            hasHit = Physics.Raycast(ray, out hit, float.MaxValue, m_canTeleportOn);
        }

        if (hasHit)
        {
            m_affected.transform.position = hit.point;
            if(d_onTeleport!=null)
                 d_onTeleport(currentPosition, hit.point, hit);
            d_onTeleportEvent.Invoke(currentPosition, hit.point, hit);
        }

        if (hasHit)
            Debug.DrawLine(ray.origin, hit.point, Color.green, 15f);
        else
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 15f);

    }

   
	void Update () {
        if (Input.GetMouseButtonDown(0)) {
            CheckForTarget();
        }
	}
}
